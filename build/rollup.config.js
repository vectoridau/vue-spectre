import VuePlugin from "rollup-plugin-vue"
import BublePlugin from "rollup-plugin-buble"
import CommonJSPlugin from 'rollup-plugin-commonjs';
import { terser } from 'rollup-plugin-terser';

export default {
  input: "src/wrapper.js",
  output: {
    name: "VueSpectre",
    exports: "named",
  },
  plugins: [
    CommonJSPlugin(),
    VuePlugin( {
      css: true,
    } ),
    BublePlugin(),
    terser(),
  ],
}
