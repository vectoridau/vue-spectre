# vue-spectre

Vue components for the Spectre.css framework.



# State

This package is currently in active development.

All `layout` components have been created, and `element` components are
currently being worked on.

Documentation will be prettied up, but all of the information that you
need should be present, if a little ugly, for the time being.




# Compatibility

This package is designed against:

* Vue 2.0
* Spectre CSS 0.5.8

This package may work with older or newer versions - try your luck!



# Usage

## As a Node Module

### Install With Yarn

```sh
yarn add @vectoridau/vue-spectre
```

### Install With NPM

```sh
npm install @vectoridau/vue-spectre
```


## As a Standalone Javascript File

```html
<script type="text/javascript" src="vue-spectre/dist/vue-spectre.min.js"></script>
```


# Common Component Attributes

The following HTML attributes are available in all components that extend the
`SpectreBase` component.

## Responsive Visibility
* `show-xs` Show on viewports of at least extra-small size.
* `show-sm` Show on viewports of at least small size.
* `show-md` Show on viewports of at least medium size.
* `show-lg` Show on viewports of at least large size.
* `show-xl` Show on viewports of at least extra-large size.

Example:

```html
<container show-lg>
  <p>
    This container will not be visible on extra-small, small, or
    medium viewports.
  </p>
</container>
```

## Responsive Invisibility
* `hide-xs` Hide on viewports at least as small as an extra-small viewport.
* `hide-sm` Hide on viewports at least as small as a small viewport.
* `hide-md` Hide on viewports at least as small as a medium viewport.
* `hide-lg` Hide on viewports at least as small as a large viewport.
* `hide-xl` Hide on viewports at least as small as an extra-large viewport.

Example:

```html
<container hide-sm>
  <p>
    This container will not be visible on extra-small or small viewports.
  </p>
</container>
```

## Color

`fg`

Set the text color.

Available values:

* `primary`, `secondary`, `dark`, `gray`, `light`, `success`, `warning`, `error`

Example:

```html
<container fg="primary">
  <p>
    The text in this container will usually be green, unless you have
    modified your Spectre theme!
  </p>
</container>
```


`bg`

Set the background color.

Available values: `primary`, `secondary`, `dark`, `gray`, `success`, `warning`, `error`

Example:

```html
<container bg="dark">
  <p>
    This container will have a dark background. Setting the background
    will also set the text color to something sensible by default.
  </p>
</container>
```


## `<columns>`

Extends `SpectreBase`.

Display content in a series of columns.

Example:

```html
<columns>
  <column>Left</column>
  <column>Middle</column>
  <column>Right</column>
</columns>
```

### Component Attributes

#### `gapless`

Yield columns without gaps between each column.

Example:

```html
<columns gapless>
  <!-- ... -->
</columns>
```

#### `oneline`

Force all columns to be positioned in the same row.

Example:

```html
<columns oneline>
  <!-- ... -->
</columns>
```


## `<column>`

Extends `SpectreBase`.

Display a column of content.

Example:

```html
<columns>
  <column>Left</column>
  <column>Middle</column>
  <column>Right</column>
</columns>
```

### Component Attributes

#### `w`

Specify the width that this column should occupy.

Available values: `auto`, `1`, `2`, `3`, `4`, `5`, `6`, `7`, `8`,
                  `9`, `10`, `11`, `12`

Example:

```html
<columns>
  <column w="4">4-wide column</column>
  <column w="5">5-wide column</column>
  <column w="3">3-wide column</column>
</columns>
```

Notes:

`auto` width columns will take up the *minimum* space that they require,
leaving the rest of the available space for other columns.


### `xs`, `sm`, `md`, `lg`, `xl`

Specify the width that this column should occupy when the view port is
at most the given size. The usage of these attributes is otherwise identical
to the `w` attribute.


### `ml`, `mx`, and `mr`

Set automatic margins to the left (`ml`), the right (`mr`), or bot the left
and right (`mx`) of the column.

Example:

```html
<columns>
  <column w="2">
    <div class="block bg-secondary">col-2</div>
  </column>
  <column w="4" mx>
    <div class="block bg-secondary">col-4 col-mx-auto</div>
  </column>
</columns>

<columns>
  <column w="2">
    <div class="block bg-secondary">col-2</div>
  </column>
  <column w="4" ml>
    <div class="block bg-secondary">col-4 col-ml-auto</div>
  </column>
</columns>

<columns>
  <column w="4" ml>
    <div class="block bg-secondary">col-4 col-ml-auto</div>
  </column>
</columns>

<columns>
  <column w="4" mx>
    <div class="block bg-secondary">col-4 col-mx-auto</div>
  </column>
</columns>

<columns>
  <column w="4" mr>
    <div class="block bg-secondary">col-4 mr</div>
  </column>
</columns>
```


## `<hero>`

Extends `SpectreBase`.

Display a "hero" block, designed to attract attention.

Example:

```html
<hero title="Attention!">
  <p>
    This is a <em>Hero</em> element!
  </p>
</hero>
```

### Component Attributes

#### `title`

The title to display at the start of the hero block.


### `sm` and `lg`

Make the hero block a small hero block (`sm`) or a large hero block (`lg`).

Example:

```html
<hero sm>
  <p>
    This is a <em>small</em> hero block.
  </p>
</hero>
```



## `<navbar>`

Extends `SpectreBase`.

Display a navbar which can be used for a header and general navigation.


Example:

```html
<navbar>
  <navbar-section>
    <a href="#one">Section One</a>
    <a href="#two">Section Two</a>
    <a href="#three">Section Three</a>
  </navbar-section>
</navbar>
```


### Component Slots

The default slot contains the hero content.

The `title` slot overrides the default title element.



## `<navbar-section>`

Extends `SpectreBase`.

Display a section of a navbar. Navbar sections are used to group content
to the left, center and right of the navbar.

Using a single `<navbar-section>` element within a navbar will yield a single
section on the left.

Using two consecutive `<navbar-section>` elements within a navbar will yield
a left and right section.

Using three consecutive `<navbar-section>` elements within a navbar will yield
a left, center, and right section, if you also add the `center` attribute to
the second `<navbar-section>` element.

Example:

```html
<navbar>
  <navbar-section>Left Menu</navbar-section>
  <navbar-section center>Center Menu</navbar-section>
  <navbar-section>Right Menu</navbar-section>
</navbar>
```

### Component Attributes

#### `center`

This attribute should be used to denote the central navbar section, if one is
present. This will distinguish the central section from left or right sections
and allow it to be placed correctly.

Example:

```html
<navbar-section center>
  <!-- ... -->
</navbar-section>
```


## `<btn>`

Create a button.

### Component Attributes

#### `sm` and `lg`

Make the button small (`sm`) or large (`lg`).

Example:

```html
<btn sm>This is a small button</btn>
<btn lg>This is a large button</btn>
```


#### `primary`, `link`, `success`, and `error`

Apply a visual style to the button.

Example:

```html
<btn primary>Primary Button</btn>
<btn link>Link Button</btn>
<btn success>Success Button</btn>
<btn error>Error Button</btn>
```


#### `block`

Make the button full-width.

Example:

```html
<btn block>This is a full-width button</btn>
```


#### `action` and `circle`

Make the button square (`action`) or circular (`circle`).

```html
<btn square>S</btn>
<btn circle>C</btn>
```


#### `active`

Set the button's visual state to its active style.

```html
<btn active>This is an active button</btn>
```


#### `disabled`

Disable the button and set its visual state to match.

```html
<btn disabled>This is an inactive button</btn>
```



#### `loading`

Replace the button text with an animated loading indicator.

```html
<btn loading>This text will not be shown</btn>
```



## `<btn-group>`

Present a group of buttons attached together.

Example:

```html
<btn-group>
  <btn primary>Main Action</btn>
  <btn>Secondary Action</btn>
  <btn>Tertiary Action</btn>
</btn-group>
```

### Component Attributes

#### `block`

Make the button group full-width.

Example:

```html
<btn-group block>
  <btn>These Buttons</btn>
  <btn>Will Stretch</btn>
  <btn>To Fit</btn>
</btn-group>
```


## `<input-text>`

Display a nice-looking text input.
This input can be bound as a v-model, and display an associated label.

Example:

```html
<template>
  <container>
    <input-text v-model="name" placeholder="Name" label="Name"></input-text>
    Your name is: {{ name }}
  </container>
</template>
<script>
  export default {
    data: function() {
      return {
        name: ""
      };
    }
  }
</script>
```

### Component Attributes

#### `v-model`

Bind a vue data model to this input.


### `id`

Set the HTML ID for the text input (and associated label if present).


### `name`

Set the form element name for the text input.


### `value`

Set the value of the text input.

This attribute is used as part of the `v-model` binding process. **You should
not use this attribute if you are also using `v-model` on this element.**


### `type`

Set the type of the underlying form input. You can specify any type you want,
but you should probably limit it to the basic "text-related" types, such as:

* `text`
* `password`
* `email`


### `placeholder`

The placeholder to display on the form input when the user has not supplied
any text.


### `maxlength`

The maximum number of characters allowed in this field.


### `required`

Force this input to be required in the context of submitting a form.


### `readonly`

Prevent the content of this input from being modified.


### `disabled`

Prevent this input from being selected or focused on. Also update the visual
appearance of this input to represent the state.


### `formnovalidate`

Prevent standard form validation rules applying to this input in the context
of submitting a form.


### `autocomplete` and `no-autocomplete`

Enable or disable autocomplete for this input.

Example:

```html
<!-- Enable autocomplete -->
<input-text autocomplete ...></input-text>

<!-- Disable autocomplete -->
<input-text no-autocomplete ...></input-text>

<!-- Enable autocomplete and specify autocomplete field types -->
<input-text v-model="firstName" autocomplete="fname"></input-text>
<input-text v-model="lastName" autocomplete="lname"></input-text>
<input-text v-model="email" autocomplete="email"></input-text>
<input-text v-model="creditCardName" autocomplete="cc-name"></input-text>
<input-text v-model="creditCardNumber" autocomplete="cc-number"></input-text>
<input-text v-model="creditCardCVC" autocomplete="cc-csc"></input-text>
<input-text v-model="creditCardExpiry" autocomplete="cc-exp"></input-text>
```


### `autofocus`

Automatically focus on this input once the page loads.


### `pattern`

Specify a regular expression pattern that should be used to validate the
content of this input.



### Component Events

#### `@input => function( newValue )`

Raised when the value of this element changes.

This event is used as part of the `v-model` binding process. You may use
this event even if you are also using `v-model` on this element.



## `<input-textarea>`

Documentation to come, but mostly the same as `<input-text>`.
Main differences:

* No `type` attribute
* Addition of `cols` attribute
* Addition of `rows` attribute



# Author

Daniel "Vector" Kerr <vector@vector.id.au>
