import Columns from './components/layout/Columns.vue'
import Column from './components/layout/Column.vue'
import Container from './components/layout/Container.vue'
import Hero from './components/layout/Hero.vue'
import Navbar from './components/layout/Navbar.vue'
import NavbarSection from './components/layout/NavbarSection.vue'

import Btn from './components/elements/Btn.vue'
import BtnGroup from './components/elements/BtnGroup.vue'

import InputText from './components/elements/form/InputText.vue'
import InputTextarea from './components/elements/form/InputTextarea.vue'



// Plugin
const VueSpectre = {
  // components: {
  //   Columns,
  //   Column,
  //   Container,
  // }
  install( Vue, options ) {
    Vue.component( Columns.name, Columns );
    Vue.component( Column.name, Column );
    Vue.component( Container.name, Container );
    Vue.component( Hero.name, Hero );
    Vue.component( Navbar.name, Navbar );
    Vue.component( NavbarSection.name, NavbarSection );

    Vue.component( Btn.name, Btn );
    Vue.component( BtnGroup.name, BtnGroup );

    Vue.component( InputText.name, InputText );
    Vue.component( InputTextarea.name, InputTextarea );
  }
}

// Global Namespace (browser-based include)
if( typeof window !== 'undefined' && window.Vue ) {
  window.Vue.use( VueSpectre )
}

// Plugin Module Export
export default VueSpectre;
